package com.hackathon.otp.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import com.hackathon.otp.dto.OtpRequest;
import com.hackathon.otp.dto.OtpResponse;
import com.hackathon.otp.service.impl.OtpServiceImpl;

@RunWith(MockitoJUnitRunner.Silent.class)
public class OtpControllerTest {

	@Mock
	OtpServiceImpl otpService;
	
	@InjectMocks
	OtpController otpController;
	
	
	OtpRequest otpRequest;
	OtpResponse otpResponse;
	
	
	@Before
	public void setup() {
		otpRequest = new OtpRequest();
		otpResponse = new OtpResponse();
		otpResponse.setOtp("12456");
		otpResponse.setResponse("Success");
		
	}
	
	@Test
	public void testGenerateOtp() {
		Mockito.when(otpService.generateOtp(Mockito.any())).thenReturn(otpResponse);
		assertEquals(otpResponse, otpController.generateOtp(otpRequest).getBody());
	}
	
	@Test
	public void testGenerateOtpNegative() {
		Mockito.when(otpService.generateOtp(Mockito.any())).thenReturn(null);
		assertEquals(null, otpController.generateOtp(otpRequest).getBody());
	}
}
