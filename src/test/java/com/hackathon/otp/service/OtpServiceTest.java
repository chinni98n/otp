package com.hackathon.otp.service;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.hackathon.otp.dto.OtpRequest;
import com.hackathon.otp.dto.OtpResponse;
import com.hackathon.otp.exception.InvalidRequestTypeException;
import com.hackathon.otp.repository.OtpRepo;
import com.hackathon.otp.service.impl.OtpServiceImpl;

@RunWith(MockitoJUnitRunner.Silent.class)
public class OtpServiceTest {

	@Mock
	OtpRepo otpRepo;
	
	@InjectMocks
	OtpServiceImpl otpService;
	
	OtpRequest otpRequest;
	
	@Before
	public void setup() {
		otpRequest = new OtpRequest();
		otpRequest.setUserId(1L);
		otpRequest.setType("payment");
	}
	
	@Test
	public void testGenerateOtp() {
		OtpResponse otpResponse = otpService.generateOtp(otpRequest);
		assertNotNull(otpResponse);
	}
	
	
	@Test
	public void testGenerateOtp2() {
		otpRequest.setType("PASSWORD RESET");
		OtpResponse otpResponse = otpService.generateOtp(otpRequest);
		assertNotNull(otpResponse);
	}
	
	@Test(expected = InvalidRequestTypeException.class)
	public void testGenerateOtpNegative() {
		otpRequest.setType("sdjn");
		otpService.generateOtp(otpRequest);
	}
}
