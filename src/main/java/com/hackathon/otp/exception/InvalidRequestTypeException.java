package com.hackathon.otp.exception;

public class InvalidRequestTypeException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidRequestTypeException() {
		super();
	}

	public InvalidRequestTypeException(String message) {
		super(message);
		
	}
	

}
