package com.hackathon.otp.exception;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.hackathon.otp.constants.ApplicationConstants;

@RestControllerAdvice
public class ExceptionHandlerControllerAdvice {

	@ExceptionHandler(InvalidRequestTypeException.class)
	public final ExceptionResponse handleInvalidRequestTypeException(Exception exception, WebRequest request) {
		ExceptionResponse response  = new ExceptionResponse();
		response.setStatus(ApplicationConstants.INVALID_TYPE_CODE);
		response.setMessage(exception.getMessage());
		return response;
	}
	
	@ExceptionHandler(RecordNotFoundException.class)
	public final ExceptionResponse handleRecordNotFoundExceptionn(Exception exception, WebRequest request) {
		ExceptionResponse response  = new ExceptionResponse();
		response.setStatus(ApplicationConstants.RECORD_NOT_FOUND_CODE);
		response.setMessage(exception.getMessage());
		return response;
	}
}
