package com.hackathon.otp.exception;

public class OtpInvalidException extends RuntimeException{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OtpInvalidException() {
		super();
	}

	public OtpInvalidException(String message) {
		super(message);
		
	}
	
}
