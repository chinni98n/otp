package com.hackathon.otp.exception;

import lombok.Data;

@Data
public class ExceptionResponse {

	private Integer status;
	private String message;
}
