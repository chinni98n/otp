package com.hackathon.otp.service.impl;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hackathon.otp.constants.ApplicationConstants;
import com.hackathon.otp.dto.OtpRequest;
import com.hackathon.otp.dto.OtpResponse;
import com.hackathon.otp.dto.ResendOtpRequest;
import com.hackathon.otp.dto.ResponseDto;
import com.hackathon.otp.dto.ValidateOtpRequest;
import com.hackathon.otp.entity.Otp;
import com.hackathon.otp.exception.BadRequestException;
import com.hackathon.otp.exception.InvalidRequestTypeException;
import com.hackathon.otp.exception.OtpInvalidException;
import com.hackathon.otp.exception.RecordNotFoundException;
import com.hackathon.otp.repository.OtpRepo;
import com.hackathon.otp.service.OtpService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class OtpServiceImpl implements OtpService {

	@Autowired
	OtpRepo otpRepo;
	
	
	@Override
	public OtpResponse generateOtp(OtpRequest otpRequest) {

		log.info("Generating Otp");
		String type = otpRequest.getType();
		String otp = "";
		if (type.equalsIgnoreCase("payment")) {
			otp = this.generateOtpforPayment();
		} else if(type.equalsIgnoreCase("password reset")){
			otp = this.generateOtpforPasswordReset();
		}
		else {
			throw new InvalidRequestTypeException(ApplicationConstants.INVALID_TYPE);
		}

		log.debug("Store details to database");
		LocalDateTime createTime = LocalDateTime.now();
		LocalDateTime expiryTime = createTime.plusMinutes(1);
		Otp otpObj = new Otp();
		otpObj.setUserId(otpRequest.getUserId());
		otpObj.setCreateTime(createTime);
		otpObj.setExpiryTime(expiryTime);
		otpObj.setOtp(otp);
		otpObj.setType(type);
		otpRepo.save(otpObj);
		
		OtpResponse otpResponse = new OtpResponse();
		otpResponse.setOtp(otp);
		otpResponse.setResponse(ApplicationConstants.OTP_MESSAGE);

		return otpResponse;
	}

	private String generateOtpforPasswordReset() {
		log.info("Generates otp for password Reset");
		Random random = new Random();
		String accepted_digits = "0123456789";
		String accepted_caps = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String accepted_small = "abcdefghijklmnopqrstuvwxyz";
		StringBuilder str = new StringBuilder(accepted_digits).append(accepted_caps).append(accepted_small);
		String accepted_chars = str.toString();
		char[] otp = new char[8];
		for (int i = 0; i < 8; i++) {
			otp[i] = accepted_chars.charAt(random.nextInt(accepted_chars.length()));
		}
		log.info("otp is : " + String.valueOf(otp));
		return String.valueOf(otp);
	}

	private String generateOtpforPayment() {
		
		log.info("Generates otp for payment");
		Random random = new Random();
		String accepted_chars = "0123456789";
		char[] otp = new char[6];
		for (int i = 0; i < 6; i++) {
			otp[i] = accepted_chars.charAt(random.nextInt(accepted_chars.length()));
		}
		log.info("otp is : " + String.valueOf(otp));
		return String.valueOf(otp);
	}

	@Override
	public ResponseDto validateOtp(ValidateOtpRequest otpRequest) {
		String otpString = otpRequest.getOtp();
		Long givenUserId = otpRequest.getUserId();
		LocalDateTime requestedTime = otpRequest.getTime();
		Optional<Otp> otpOpt = otpRepo.findByUserId(givenUserId);
		if(!otpOpt.isPresent())
			throw new RecordNotFoundException(ApplicationConstants.RECORD_NOT_FOUND);
		Otp otp = otpOpt.get();
		Long actualuserId = otp.getUserId();
		if(actualuserId!=givenUserId)
			throw new BadRequestException(ApplicationConstants.INVALID_REQUEST);
		
		LocalDateTime expiryTime = otp.getExpiryTime();
		if(requestedTime.isAfter(expiryTime) || otpString!=otp.getOtp())
			throw new OtpInvalidException(ApplicationConstants.INVALID_OTP);
		String type = otp.getType();
		ResponseDto response = new ResponseDto();
		if(type.equalsIgnoreCase("payment")) {
			response.setStatus(ApplicationConstants.PAYMENT_SUCCESS_CODE);
			response.setMessage(ApplicationConstants.PAYMENT_SUCCESS);
		}
		else {
			response.setStatus(ApplicationConstants.PASSWORD_CHANGE_SUCCESS_CODE);
			response.setMessage(ApplicationConstants.PASSWORD_CHANGE_SUCCESS);
		}
		return response;
	}

	@Override
	public OtpResponse resendOtp(ResendOtpRequest otpRequest) {
		Long otpId = otpRequest.getOtpId();
		Optional<Otp> otpOpt = otpRepo.findById(otpId);
		if(!otpOpt.isPresent())
			throw new RecordNotFoundException(ApplicationConstants.RECORD_NOT_FOUND);
		
		Otp otpObj = otpOpt.get();
		LocalDateTime createTime = otpObj.getCreateTime();
		LocalDateTime expiryTime = otpObj.getExpiryTime();
		LocalDateTime requestTime = otpRequest.getTime();
		if(requestTime.isBefore(createTime.plusSeconds(15)) || requestTime.isAfter(expiryTime))
			throw new BadRequestException(ApplicationConstants.INVALID_REQUEST);
		
		String otp = otpObj.getOtp();
		OtpResponse otpResponse = new OtpResponse();
		otpResponse.setOtp(otp);
		otpResponse.setResponse(ApplicationConstants.REQUEST_SUCCESS);
		
		return otpResponse;
	}

}
