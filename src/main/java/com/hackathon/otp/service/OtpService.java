package com.hackathon.otp.service;

import com.hackathon.otp.dto.OtpRequest;
import com.hackathon.otp.dto.OtpResponse;
import com.hackathon.otp.dto.ResendOtpRequest;
import com.hackathon.otp.dto.ResponseDto;
import com.hackathon.otp.dto.ValidateOtpRequest;

public interface OtpService {

	public OtpResponse generateOtp(OtpRequest otpRequest);
	public ResponseDto validateOtp(ValidateOtpRequest otpRequest);
	public OtpResponse resendOtp(ResendOtpRequest otpRequest);
	
}
