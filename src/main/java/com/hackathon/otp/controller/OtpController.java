package com.hackathon.otp.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hackathon.otp.dto.OtpRequest;
import com.hackathon.otp.dto.OtpResponse;
import com.hackathon.otp.dto.ResendOtpRequest;
import com.hackathon.otp.dto.ResponseDto;
import com.hackathon.otp.dto.ValidateOtpRequest;
import com.hackathon.otp.service.impl.OtpServiceImpl;

@RestController
public class OtpController {

	@Autowired
	OtpServiceImpl otpService;
	
	@PostMapping("/generateOtp")
	public ResponseEntity<OtpResponse> generateOtp(@RequestBody @Valid OtpRequest otpRequest){
		OtpResponse response = otpService.generateOtp(otpRequest);
		return new ResponseEntity<>(response,HttpStatus.OK);
	}
	@PostMapping("/validateOtp")
	public ResponseEntity<ResponseDto> validateOtp(@RequestBody @Valid ValidateOtpRequest otpRequest){
		ResponseDto response = otpService.validateOtp(otpRequest);
		return new ResponseEntity<>(response,HttpStatus.OK);
	}
	@PostMapping("/resendOtp")
	public ResponseEntity<OtpResponse> resendOtp(@RequestBody @Valid ResendOtpRequest otpRequest){
		OtpResponse response = otpService.resendOtp(otpRequest);
		return new ResponseEntity<>(response,HttpStatus.OK);
	}
}
