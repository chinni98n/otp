package com.hackathon.otp.constants;

public class ApplicationConstants {

	private ApplicationConstants() {
		super();
	}

	public static final String OTP_MESSAGE = "Your OTP expires in 60 seconds";

	public static final Integer INVALID_TYPE_CODE = 1000;
	public static final String INVALID_TYPE = "Entered type is invalid";

	public static final Integer RECORD_NOT_FOUND_CODE = 1001;
	public static final String RECORD_NOT_FOUND = "No record found";

	public static final Integer INVALID_REQUEST_CODE = 1002;
	public static final String INVALID_REQUEST = "Invalid request";

	public static final Integer INVALID_OTP_CODE = 1003;
	public static final String INVALID_OTP = "Entered OTP is invalid. Please check";

	public static final Integer PAYMENT_SUCCESS_CODE = 1004;
	public static final String PAYMENT_SUCCESS = "Payment otp is correct";

	public static final Integer PASSWORD_CHANGE_SUCCESS_CODE = 1005;
	public static final String PASSWORD_CHANGE_SUCCESS = "Password change otp is correct";

	public static final String REQUEST_SUCCESS = "Request success";	

}
