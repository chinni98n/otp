package com.hackathon.otp.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hackathon.otp.entity.Otp;


@Repository
public interface OtpRepo extends CrudRepository<Otp, Long> {

	Optional<Otp> findByUserId(Long givenUserId);

}
