package com.hackathon.otp.dto;

import java.time.LocalDateTime;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class ValidateOtpRequest {

	@NotNull
	private String otp;
	
	@Min(value = 1)
	private Long userId;
	
	@NotNull
	private LocalDateTime time = LocalDateTime.now();
}
