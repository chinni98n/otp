package com.hackathon.otp.dto;

import java.time.LocalDateTime;

import javax.validation.constraints.Min;
import lombok.Data;

@Data
public class ResendOtpRequest {

	@Min(value = 1)
	private Long userId;
	@Min(value = 1)
	private Long otpId;
	private LocalDateTime time = LocalDateTime.now();
}
