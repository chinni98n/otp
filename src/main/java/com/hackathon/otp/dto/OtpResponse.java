package com.hackathon.otp.dto;

import lombok.Data;

@Data
public class OtpResponse{

	private String otp;
	private String response;

}
