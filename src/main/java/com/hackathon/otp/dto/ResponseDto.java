package com.hackathon.otp.dto;

import lombok.Data;

@Data
public class ResponseDto {

	private Integer status;
	private String message;
}
