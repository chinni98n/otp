package com.hackathon.otp.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class OtpRequest{
	
	@Min(value = 1)
	private Long userId;
	@NotNull
	private String type;	

}
